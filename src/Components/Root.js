import React from 'react';
import '../CSS/index.css';
import Homepage from './Homepage';



class Root extends React.Component {
  constructor(props){
    super(props);
    this.changeWindow=this.changeWindow.bind(this);
    this.state ={visibleWindowRoot : "Homepage",
                orientationLandscape :window.orientation!==undefined? window.orientation == 90 || window.orientation == -90 
                                                                    : true
    } 
  }


  changeWindow(w){
    this.setState({visibleWindowRoot : w});
    console.log("visibleWindow",this.state.visibleWindowRoot)
  }

  
  
  render(){
    var self=this;
    window.addEventListener("orientationchange",(e)=>{
        var newState = Object.assign(self.state);
        newState.orientationLandscape=window.orientation == 90 || window.orientation == -90;
        self.setState(newState);
    });


 
        
  return(<div>
          {this.state.orientationLandscape
          ?
            <Homepage openWindow={this.changeWindow} visibleWindow={this.state.visibleWindowRoot}/>
          :
          <div className="portrait-page">
           
            <h1 className="text-portrait">GIRARE IL DISPOSITIVO IN MODALIT&Aacute; ORIZZONTALE</h1>
       
          </div>
          }
        </div>);
  }
}
export default Root;
