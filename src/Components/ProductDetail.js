import '../CSS/ProductDetail.css';
import React from 'react';

class ProductDetail extends React.Component {
  constructor(props){
    super(props);  
 
    this.clickCart=this.clickCart.bind(this);
    this.changeQuantity=this.changeQuantity.bind(this);



    this.state={
      quantity:1
    }
  }



  
 changeQuantity(e){
   var self=this;
   var newState = Object.assign(self.state);
   newState.quantity = e.target.value;
   this.setState(newState);
 }

  clickCart(){
    var self=this;
    if(!self.props.productInCart){
      this.props.addToCart(this.props.productProperties,this.state.quantity);
    }
    else if (this.state.quantity==0){
      this.props.removeToCart(this.props.productProperties);
    }
    else {
      this.props.removeToCart(this.props.productProperties);
      this.props.addToCart(this.props.productProperties,this.state.quantity);
    }
  }




  render(){
    
    var buttonLabel = "";
    if(!this.props.productInCart) buttonLabel = "Aggiungi al carrello";
    else if(this.state.quantity==0) buttonLabel ="Togli dal carrello";
    else buttonLabel="Aggiorna";
    

  return (
              <div className="main-div-tooltip" onMouseLeave={this.hidePayment}>
               
                    <div className ="header-detail">
                      <div className="nome-detail">
                        {this.props.productProperties.nome}
                      </div>
                    </div>
                    <div className ="body-detail">
                      <div className="column-left-detail">
                        <div className="image-detail-2"  style={{backgroundImage: "url("+this.props.productProperties.immagine2+")"}}/>
                        <div className="image-detail-3"  style={{backgroundImage: "url("+this.props.productProperties.immagine3+")"}}/>
                        {/* <div className="riquadro-specifiche">
                          <u>Codice:</u> {this.props.productProperties.codice} <br/>
                          <u>Categoria:</u>  {this.props.productProperties.categorie} <br/>
                          <u>Brand:</u>  {this.props.productProperties.brand}<br/>
                          <u>Dimensioni:</u>   {this.props.productProperties.dimensioni}<br/>      
                          <u>Peso:</u> {this.props.productProperties.peso} <br/>                          
                        </div> */}
                      
                      
                      </div>
                      <div className="column-right-detail">
                        <div className="description-detail"> 
                            {this.props.productProperties.descrizione}  
                        </div>
                        <div className="riquadro-specifiche">
                          <u>Codice:</u> {this.props.productProperties.codice} <br/>
                          <u>Categoria:</u>  {this.props.productProperties.categorie} <br/>
                          <u>Brand:</u>  {this.props.productProperties.brand}<br/>
                          <u>Dimensioni:</u>   {this.props.productProperties.dimensioni}<br/>      
                          <u>Peso:</u> {this.props.productProperties.peso} <br/>                          
                        </div>  
                        <div className="quantity-select">
                          <div id = "quantityLabel">Quantità</div>
                          <input type="number" id="quantitySelect" min="0" max="1000" defaultValue="1" onChange={this.changeQuantity}></input>
                        </div>
                        <input type="button" value={buttonLabel} className="add-to-cart-button" 
                           onMouseOver={this.showPayment} onClick={this.clickCart}/>
                        <span  style={{display:"inline-block"}}>
                          <div className="prezzo-detail">
                              {this.props.productProperties.prezzo}
                              <div className="euro-symbol-detail">€</div>
                          </div>
                        </span>
                      </div>

                    </div>                   
            </div>
  );
}
}
export default ProductDetail;

