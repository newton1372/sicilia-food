import provinceItaliane from '../Properties/Provinceitaliane'
import React from 'react';



class EmailTemplate extends React.Component {
  constructor(props){
    super(props);  
    this.state = {
      infoNotification: this.props.infoNotification
    }
  }


  componentWillReceiveProps(prop){
    this.setState({infoNotification: this.props.infoNotification});
  }



  abbreviaProvincia = (provinciaIntera) => {
    var provincia = provinceItaliane.filter((p)=>{
        return p.nome === provinciaIntera
    })[0];
    return provincia.sigla;
  }
    
  render(){
   var info = this.state.infoNotification;
  
  
   return(
        (info && info.provincia) &&
          <div> 
            <h1>Complimenti!</h1>
            <h2>Hai appena acquistato i seguenti prodotti:</h2>
            {
            this.props.carrello.map((product)=>{
              return(<div>
                      <div>{product.nome}</div>
                      <div>Cod. {product.codice}</div>
                      <div>{product.prezzo}€</div>                               
                      <div>&#215;{product.quantityCart} = {(product.prezzo*product.quantityCart).toFixed(2)}€</div>
                      <hr/>
                    </div>     
            )})
            
            }

            <h3>Totale: {this.props.calculateTotalCart()}€</h3>

            <div>
                <h4>INDIRIZZO DI SPEDIZIONE:</h4>
                <div>
                    <div>{info.indirizzo}, {info.comune} ({this.abbreviaProvincia(info.provincia)})</div>
                    <div>{info.cap}</div>
                </div>
            </div>
            <div>               
                <h4>NOTE:</h4>      
                <div>
                    <div>{info.note}</div>
                </div>
            </div>

            <div>       
                <h4>SPEDIZIONE:</h4>      
                <div>
                    <div>{info.spedizione}</div>
                </div>
                {
                  <div>
                  {info.spedizione==="Corriere" && <div>Consegna:2-3 giorni lavorativi con corriere GLS</div>}
              </div>
                }
            </div>

            
    </div> 

        );
    }
  }
export default EmailTemplate;

