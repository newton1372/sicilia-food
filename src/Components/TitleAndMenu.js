import '../CSS/Homepage.css';
import MenuComponent from './MenuComponent';
import MenuList from '../Properties/MenuList';
import React from 'react';

class TitleAndMenu extends React.Component {


  render(){
    
    return (      
        <div >
          <header id="headerTitolo">
            <div className="titolo">
              
             </div>                 
          </header>
          <span className="menu-context-class">                        
                {    
                  MenuList.listMenu.map(function(item){                        
                  return (                             
                        <MenuComponent type={item}/>
                  )
                  })
                }    
          </span>      

      </div>);
  }

}
export default TitleAndMenu;
