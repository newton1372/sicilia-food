import '../CSS/Contatti.css';
import React from 'react';
import ContattiProperties from "../Properties/ContattiProperties";
class ContattiTooltip extends React.Component {

  constructor(props){
    super(props);
    this.sendMail = this.sendMail.bind(this);
  }



  sendMail(){


  }


  render(){
    
        

  return (
              <div className="contatti-tooltip-container" onMouseOver={this.props.openParentMethod} onMouseLeave={this.props.closeParentMethod}>
                <div className="logo-contatti"></div>

                <div className="block-contatto">      
                    <div className="facebook-logo"/>{/* 
                     */}<div className="label-contatto">{ContattiProperties.facebook}</div>
                </div>
                <div className="block-contatto">
                  <div className="whatsapp-logo"/>{/* 
                     */}<div className="label-contatto">{ContattiProperties.whatsapp}</div>
                </div>
                <div className="block-contatto">
                  <div className="email-logo"/>{/* 
                     */}<div className="label-contatto">
                       <a href={"mailto:"+ContattiProperties.email}>{ContattiProperties.email}</a>
                  </div>
                </div>
                <div className="block-contatto">
                  <div className="cell-logo"/>{/* 
                     */}<div className="label-contatto"><a>{ContattiProperties.cell}</a></div>
                </div>
            </div>
  );
}
}
export default ContattiTooltip;

