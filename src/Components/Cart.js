import '../CSS/Cart.css';
import PaymentModule from './PaymentModule'
import PayPalProperties from '../Properties/PayPalProperties'
import Properties from '../Properties/NetworkProperties'
import React from 'react';
import ReactDOMServer from 'react-dom/server'
import {PayPalButton} from 'react-paypal-button-v2';
import EmailTemplate from './EmailTemplate';

class Cart extends React.Component {
  constructor(props){
    super(props);  
    this.calculateTotal=this.calculateTotal.bind(this);
    this.changePaymentSelected=this.changePaymentSelected.bind(this);
    this.toggleSumUpCard=this.toggleSumUpCard.bind(this);
    this.setInfoNotification=this.setInfoNotification.bind(this);
    this.paymentSuccessfullLogic=this.paymentSuccessfullLogic.bind(this);
    this.sendMail=this.sendMail.bind(this);
    this.addTransactionToServer=this.addTransactionToServer.bind(this);
    this.togliProdottoDaCarrello=this.togliProdottoDaCarrello.bind(this);


    this.state={
      carrello:[],
      paymentSelected:"PayPal",
      sumUpVisible:false,
      infoNotification:null
    }

  }

  

  setInfoNotification(obj){
    var self=this;
    var newState = Object.assign(self.state);
    newState.infoNotification =obj;
    this.setState(newState);
  }


  calculateTotal(){
    var price=0;
     console.log(this.state.carrello)
    this.state.carrello.forEach(function(item){
     
      price += parseFloat(item.prezzo*item.quantityCart);
    })
    return price.toFixed(2);
  }




  componentWillReceiveProps(newProps){
    var self=this;
    var newState = Object.assign(self.state);
    newState.carrello = newProps.carrello;
    this.setState(newState);
    const spedizioneProduct = {nome:"Corriere", codice:"GLS", prezzo:"10.00",quantityCart:1};
   
    if(this.state.infoNotification && newProps.carrello!==undefined && this.state.infoNotification.spedizione==="Corriere" 
      && !newProps.carrello.some((e)=>{return e.nome==="Corriere"})){
         var carrelloWithCorriere = Object.assign(this.state.carrello);
         carrelloWithCorriere.push(spedizioneProduct);
         this.setState({carrello: carrelloWithCorriere});
            
    }

    if(this.state.infoNotification && this.state.infoNotification.spedizione==="Ritiro in negozio") {
      var carrelloWithoutCorriere = newProps.carrello.filter((obj)=>{return obj.nome !== "Corriere"});
      this.setState({carrello:carrelloWithoutCorriere});
    }

  }
  



 componentDidMount(){
  window.SumUpCard.mount({
    checkoutId: '2ceffb63-cbbe-4227-87cf-0409dd191a98',
    onResponse: function(type, body) {
        console.log('Type', type);
        console.log('Body', body);
    }
  });
 }



 changePaymentSelected(str){
  var self=this;
  var newState = Object.assign(self.state);
  newState.paymentSelected = str;
  this.setState(newState);
 }


 toggleSumUpCard(){
  var self=this;
  var newState = Object.assign(self.state);
  newState.sumUpVisible = !newState.sumUpVisible;
  this.setState(newState);
 }



 sendMail(htmlString,recipientString){
  const url_mail_service="http://"+Properties.public_ip+":"+Properties.port_db_app+"/sendMail";
  fetch(url_mail_service,{
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers: {
      'Content-Type': 'application/json'
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer',
    body: JSON.stringify({
      html: htmlString,
      fromMail: "siciliafoodrosolini@virgilio.it",
      toMail: recipientString
    })
  }).then((resp)=> {
    resp.text().then((respText)=>{
    if(respText==="OK") console.log("Mail inviata con successo!");
    else alert("Invio mail fallita");
  });
 });
   
 }




  paymentSuccessfullLogic(details,data){
    var self=this;
    var emailBody = <EmailTemplate carrello={this.state.carrello} infoNotification={this.state.infoNotification} calculateTotalCart={this.calculateTotal}/>
    var htmlEmailString = ReactDOMServer.renderToString(emailBody);
    console.log(htmlEmailString);
    this.sendMail(htmlEmailString,"siciliafoodrosolini@virgilio.it");
    this.sendMail(htmlEmailString,self.state.infoNotification.email);

    var dataToSend = {
      infoNotification: this.state.infoNotification,
      details:details,
      data:data
    }  

 }






 addTransactionToServer(details,data){
  var objectToRegister={
    details:details,
    data:data,
    infoBuyer:this.state.infoNotification
  }

  const url_transaction_service="http://"+Properties.public_ip+":"+Properties.port_db_app+"/addTransaction";
  fetch(url_transaction_service,{
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers: {
      'Content-Type': 'application/json'
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer',
    body: JSON.stringify(objectToRegister)
  }).then((resp)=> {
    resp.text().then((respText)=>{
    if(respText==="OK") console.log("La transazione è stata registrata!");
    else alert("La transazione non è stata registrata");
  });
 });
 }
 


 togliProdottoDaCarrello(obj){
    var C= this.state.carrello.filter((x)=>{return obj.codice !== x.codice});
    this.props.setCarrello(C);
 }
 
  render(){
    var self=this;
    var total =this.calculateTotal();
    var paymentButton =<div/>
    var sumupClassVisibility= self.state.sumUpVisible? "sumup-card-visible" : "sumup-card-invisible";
    

    switch(self.state.paymentSelected){
        case "PayPal": paymentButton=
                                <div className="button-paypal-container"> 
                                  <PayPalButton
                                      
                                      amount={total}
                                      // shippingPreference="NO_SHIPPING" // default is "GET_FROM_FILE"
                                      createOrder={(data, actions) => {
                                        return actions.order.create({
                                          purchase_units: [{
                                            amount: {
                                              currency_code: "EUR",
                                              value:  total
                                            }
                                          }],
                                        });
                                      }}
                                      onSuccess={(details, data) => {
                                        alert("Pagamento effettuato correttamente!");        
                                        self.paymentSuccessfullLogic(details,data);
                                        self.addTransactionToServer(details,data);
                                      }}

                                      onError = {(details,data) =>{
                                        alert("Transazione fallita. Il pagamento non è stato effettuato");
                                        console.log("DETAILS",details);
                                        self.addTransactionToServer(details,data);
                                      }}
                                      
                                      //options={{clientId:"AYvefKxfVqGbAPLGYFu7A7Q9iut-CFF7hgvjvJz8I_x-VBL-uPfZw7AidCLM52b-nsuPqoVPSrkgyeOs", currency:"EUR"}}
                                      options={{clientId: PayPalProperties.clientId, currency:"EUR"}}   
                                      //options={{clientId:"sb", currency:"EUR"}}

                                      style={{ size:'responsive',layout:"horizontal",color:"gold"}}                      
                                  />
                            </div>;
                            break;
            case "SumUp":   paymentButton =  <button className="sumup-button-payment-cart" value="SumUp" onClick={this.toggleSumUpCard}>Sum Up</button> 
                                          
                            break;
            default: paymentButton = <div/>

        }
                            




    return ( 
              <div className="cart-content">                                         
                    <div className="titolo-pagamento"><p>Carrello</p></div>
                     
                    <div className="cart-product-list">
                      {self.state.carrello.length==0 && 
                            <div className="carrello-vuoto-text">IL CARRELLO &Eacute; VUOTO</div>
                            }
                      
                      {
                        self.state.carrello.map(function(item){
                          return(
                            <div className="product-div-cart"> 
                                <div className="nome-cart">{item.nome}</div>
                                <div className="codice-cart">{item.codice}</div>
                                <div className="prezzo-cart">{item.prezzo}€</div> 
                                <div className="ultima-riga">  
                                <div className="delete-product" onClick={()=>self.togliProdottoDaCarrello(item)}>Togli dal carrello</div>
                                  <div className="tot">&#215;{item.quantityCart} = {(item.quantityCart*item.prezzo).toFixed(2)}€</div>
                                </div>                         
                

                            </div>             
                              )
                              })
                              }

                            </div>{/*  
                          */}<div className="total-form-payment">
                                  <PaymentModule changePaymentSelected={self.changePaymentSelected} setInfoNotification={self.setInfoNotification} />
                            </div>
                            <div className="total-price-div">
                                <div className="total-label">
                                  Totale:
                                </div>{/* 
                            */}<div className ="price">
                                {total}
                                <div className = "euro-symbol-cart">€</div>
                              </div>
                            </div>
                            {paymentButton}
                           <div id="sumup-card" className = {sumupClassVisibility} ></div>
              </div>
  );
}
}
export default Cart;

