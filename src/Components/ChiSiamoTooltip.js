import '../CSS/ChiSiamo.css';
import ChiSiamoProperties from '../Properties/ChiSiamoProperties'
import React from 'react';

class ChiSiamoTooltip extends React.Component {


  render(){
    var biography =  ChiSiamoProperties.biography;
        

  return (
            <div className="chi-siamo-tooltip-container" onMouseOver={this.props.openParentMethod} onMouseLeave={this.props.closeParentMethod}>
              <div className="title-chi-siamo">
                  Chi siamo  
              </div> 
             
             <div className="central-div-chi-siamo">
                  <div className="foto-famiglia-laterale"/>
                  {biography}
            </div>
            
            <div className="foto-famiglia-sotto">
              
            </div>
           
          </div>
  );
}
}
export default ChiSiamoTooltip;

