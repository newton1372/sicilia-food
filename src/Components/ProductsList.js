import '../CSS/ProductsList.css';
import React from 'react';
import ProductBox from './ProductBox';
import Cart from './Cart';
import ListeNozze from '../Properties/ListeNozze'
import Properties from '../Properties/NetworkProperties'


class ProductsList extends React.Component {
  constructor(props){
    super(props);    
    this.loadProducts=this.loadProducts.bind(this);
    this.mouseOverSquare=this.mouseOverSquare.bind(this);
    this.mouseLeaveSquare=this.mouseLeaveSquare.bind(this);
    this.filterByFilterString=this.filterByFilterString.bind(this);
    this.filterByListaNozze=this.filterByListaNozze.bind(this);
    this.filterPromotions=this.filterPromotions.bind(this);
    this.addProductToCart =this.addProductToCart.bind(this);
    this.removeProductToCart =this.removeProductToCart.bind(this);
    this.selectPage =this.selectPage.bind(this);
    this.togglePage =this.togglePage.bind(this);
    this.setCarrello =this.setCarrello.bind(this);
    this.toggleNavigator =this.toggleNavigator.bind(this);
    this.setPagination =this.setPagination.bind(this);
    this.nextPage =this.nextPage.bind(this);
    this.previousPage =this.previousPage.bind(this);
    this.showCart =this.showCart.bind(this);
    this.hideCart =this.hideCart.bind(this);
    this.deselectPagesNav =this.deselectPagesNav.bind(this);



    this.state={
      isLoaded:false,
      isCartVisible:false,
      openSquareCode:null,
      items:[], 
      filterString:"",
      filterCategory:"",
      pageSelectedLoad:1,
      pageSelectedClick:1,
      filterProm:false,
      listaNozzeCode:"",
      carrello:[],
      numeroProdottiEsposti:0

    }
  }

  


  componentWillReceiveProps(newProps){
    var self=this;
    var newState = Object.assign(self.state);
    newState.filterString = newProps.filterString;
    if(self.state.filterCategory!==newProps.filterCategory){
      newState.filterCategory = newProps.filterCategory;
      newState.pageSelectedClick=1;
      newState.pageSelectedLoad=1;
    }
    newState.listaNozzeCode = newProps.listaNozzeCode;   
    newState.filterProm = newProps.filterProm;           
    this.setState(newState);              
  }



  loadProducts(){
      console.log("Ready to log products!");
      var self=this;
      const url_db_app="http://"+Properties.public_ip+":"+Properties.port_db_app+"/readAllElements"
  //    const url_db_app="http://localhost:5000/";
      fetch(url_db_app).then(function(data){   
        data.json().then(function(result){
          var newState = Object.assign({},self.state);
          newState.items=result;
          newState.isLoaded=true;
          self.setState(newState);  
          console.log("STATE",self.state);
    });
  });
}
 
  componentDidMount(){
    this.loadProducts();
  }


  
  mouseOverSquare(code){
    var self =this;
    if(this.state.openSquareCode===null){
      var newState = Object.assign({},self.state);
      newState.openSquareCode = code;
      self.setState(newState);  
    }
  }


  setCarrello(C){
    this.setState({carrello: C})
  }


  showCart(e){
    var self=this;
    var newState = Object.assign({},self.state);
    newState.isCartVisible = true;
    this.setState(newState);  
  }

  hideCart(e){
    var self=this
    var newState = Object.assign({},self.state);
    newState.isCartVisible = false;
    this.setState(newState);  
}



  mouseLeaveSquare(code){
    var self=this;
    console.log("Event called: LEAVE SQUARE"+code);
    if(this.state.openSquareCode===code){
      var newState = Object.assign({},self.state);
      newState.openSquareCode = null;
      self.setState(newState);
    }
  }


 
  filterByFilterString(){
    var self=this;
    var arrayOfProducts=this.state.items.filter(function(thing){
        var bool=true;
        if(thing.nome) bool= thing.nome.toLowerCase().includes(self.state.filterString.toLowerCase());
        if(thing.descrizione) bool= thing.descrizione.toLowerCase().includes(self.state.filterString.toLowerCase());
        if(thing.categorie) bool = bool || thing.categorie.toLowerCase().includes(self.state.filterString.toLowerCase());
        if(!(thing.nome && thing.categorie && thing.descrizione)) bool=false;
        return bool;
    });
    return arrayOfProducts;

  }



  filterByFilterCategory(arr){
    var self=this;
    var arrayOfProducts = [];
    console.log(self.state.listaNozzeCode)
    if(self.state.listaNozzeCode===""){
          arrayOfProducts=arr.filter(function(thing){
          var bool=true;
          if(thing.categorie) bool = thing.categorie.toLowerCase().includes(self.state.filterCategory.toLowerCase());
          if(!thing.categorie) bool=false;
          return bool;
      });
    }
    else{
       arrayOfProducts = self.filterByListaNozze(arr);
    }
    return arrayOfProducts;
  }




  filterByListaNozze(arr){
    var self=this;
    var arrayOfProducts=[];
    var listaNozze = ListeNozze.filter(function(item){
      return item.password === self.state.listaNozzeCode;
    })[0];
    if(listaNozze){
      var listaNozzeProducts = listaNozze.listaProdotti;
      var product={};
      listaNozzeProducts.forEach(function(item){
            product = arr.filter(function(element){
                        return element.codice===item;
                      })[0];
            arrayOfProducts.push(product);
      });
    }
    else{     
      self.props.setListaNozzeCode("");
    }
    return arrayOfProducts;
  }




  filterPromotions(arr){
    var arrayOfProducts=arr.filter(function(thing){
        if(thing.promozione==="S") return true;
        else return false;
    });
    return arrayOfProducts;
  }



  addProductToCart(product,quantity){
      var self=this;
      var newState = Object.assign(self.state);
      var productCart = product;
      productCart.quantityCart=quantity;
      this.state.carrello.push(productCart);
      this.setState(newState);  
  }


  removeProductToCart(product){
    var self=this;
    var newState = Object.assign(self.state);
    newState.carrello=this.state.carrello.filter(function(item){
      return item !== product;
    });
    this.setState(newState);  
   
}


  selectPage(e){
    var self=this;
    var newState = Object.assign(self.state);
    newState.pageSelectedClick= e.target.value;
    newState.pageSelectedLoad=e.target.value;
    this.setState(newState);  
  }



  togglePage(e){
    var self=this;
    var newState = Object.assign(self.state);
    newState.pageSelectedLoad=e.target.value;
    this.setState(newState);
  }



  toggleNavigator(e){
    var self=this;
    console.log("TOGGLE NAV");
    if(e.target.className==="number") {
      e.target.className="number-selected";
      var newState = Object.assign(self.state);
      newState.pageSelectedLoad=self.state.pageSelectedClick;
      this.setState(newState);
    }
    else if(e.target.className==="number-selected") e.target.className="number";
  }



  setPagination(items,maxPage){
    //Mi prendo la lista di tutte le classi applicate alle varie pagine. Nel return ciclerò su tale lista per
    //costruire i productBox
    var PStyle=[];
    for(var i=0;i<maxPage;i++){
      PStyle[i] =  (this.state.pageSelectedClick==i+1 || this.state.pageSelectedLoad==i+1)? "number-selected" : "number";
    }
        
    return PStyle;
  }


  deselectPagesNav(){
    var self=this;
    var newState = Object.assign(self.state);
    newState.pageSelectedLoad="";
    this.setState(newState);

    document.getElementsByName("nav").forEach(function(navEl){
      navEl.className="number";
    });
  }


  nextPage(){
    var self=this;
    var newState = Object.assign(self.state);
    newState.pageSelectedClick++;
    newState.pageSelectedLoad++;
    this.setState(newState);
  }




  previousPage(){
    var self=this;
    var newState = Object.assign(self.state);
    newState.pageSelectedClick--;
    newState.pageSelectedLoad--;
    this.setState(newState);
  }





  render(){
    var self=this;            

    //Filtro prima per la barra di ricerca....
    var itemsString= this.filterByFilterString()?this.filterByFilterString():[];
    //..e poi per categoria
    var items =  this.filterByFilterCategory(itemsString)?this.filterByFilterCategory(itemsString):[];  
    //..e infine filtro solo le promozioni se richiesto
    if(this.state.filterProm===true){
      items = this.filterPromotions(items)? this.filterPromotions(items) : [];            
    }
    var maxPage = items.length%6==0? items.length/6 : Math.trunc(items.length/6)+1;
    var PStyle = this.setPagination(items, maxPage);      
    var pageItems = items.slice(6*(self.state.pageSelectedClick-1),6*(self.state.pageSelectedClick));
    var nextDisabled = this.state.pageSelectedClick === maxPage? true : false;
    var previousDisabled = this.state.pageSelectedClick === 1 ? true : false;
    var cartStyle =this.state.isCartVisible? {display:'block'} : {display:'none'};
    var cartNoCorriere = this.state.carrello.filter((element)=>{return element.nome !== "Corriere"});
  
  return (
            <div>
              <div className="numero-prodotti-totale">Numero prodotti: {items.length}</div>         
              <div className="riepilogo-div" onMouseOver={self.showCart} onMouseLeave = {self.hideCart}>   
                  <input type="button" className="go-to-cart-button" value="Va al carrello" onMouseOver={self.showCart} onClick={self.showCart}/>{/* 
                   */}<div className = "cart-icon"/>{/* 
                   */}<div className="numero-prodotti-carrello">{cartNoCorriere.length}</div>

                  <div className="cart-div" style={cartStyle}>
                    <Cart carrello={self.state.carrello} setCarrello={this.setCarrello}/>                    
                  </div>
              </div>

              <center>
            {  
                
                pageItems.map(function(item){
                  var itemsInCart = self.state.carrello.filter(function(product){
                    return product === item
                  })[0];              
                  var productInCart = itemsInCart? true : false;

                return (           
                  <span><ProductBox codeProductSelected={self.state.openSquareCode} productProperties={item} 
                                    mouseOverSquareEvent={self.mouseOverSquare}  mouseLeaveSquareEvent={self.mouseLeaveSquare} 
                                    addToCart={self.addProductToCart} removeToCart={self.removeProductToCart} productInCart={productInCart}/></span> );     
                  })
            }

            
            </center>

            <div className="pagination-div" onMouseLeave={this.deselectPagesNav}>
                <input name="nav" type="button" className="number"  value="<" onMouseOver={this.toggleNavigator} onMouseLeave={this.toggleNavigator} onClick={self.previousPage} disabled ={previousDisabled}/>
                <span  className="space"/>
                {
                  PStyle.map(function(style,index){
                  return(<span>
                            <input type="button"  className={style} value={index+1}  onMouseOut={self.togglePage}  onMouseOver={self.togglePage} onMouseLeave={self.togglePage} onClick={self.selectPage}/>
                            <span  className="space"/>
                          </span>);
                  })
                }
                <input name="nav" type="button" className="number" value=">" onMouseOver={this.toggleNavigator} onMouseLeave={this.toggleNavigator} onClick={self.nextPage} disabled={nextDisabled}/>
            </div>
      </div>
  );
  
  }
}
export default ProductsList;
