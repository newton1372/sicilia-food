import '../CSS/MenuComponent.css';
import ChiSiamoTooltip from './ChiSiamoTooltip';
import PartnersTooltip from './PartnersTooltip';
import PacchettiTooltip from './PacchettiTooltip';
import ContattiTooltip from './ContattiTooltip';
import React from 'react';


class MenuComponent extends React.Component {
  constructor(props){
    super(props);  
    this.openTooltip=this.openTooltip.bind(this);
    this.closeTooltip=this.closeTooltip.bind(this);

    this.state={
      isVisible:false,
      type:null,
    }
  }
  
  



  componentWillReceiveProps(newProps){
      var self=this;
      var newState = Object.assign(self.state);
      newState.type = newProps.type;
      this.setState(newState);
  }









  openTooltip(){
    var self=this;
      console.log("OPEN TOOLTIP CALLED")
      var newState = Object.assign(self.state);
      newState.isVisible = true;
      newState.typeOpen = self.state.type;
      this.setState(newState);   
  }




  closeTooltip(){
    var self=this;
    var newState = Object.assign(self.state);
    newState.isVisible = false;
    this.setState(newState);
  }


 
        

   
  render(){
    var self=this;
    var buttonClassName =self.state.isVisible? "menu-button-selected" : "menu-button-unselected";
    
    var tooltip =<span/>;
    if(this.state.type==="Chi siamo"){
      tooltip = self.state.isVisible? <ChiSiamoTooltip openParentMethod={this.openTooltip} closeParentMethod={this.closeTooltip}/> : <span/>
    }
    else if(this.state.type==="Pacchetti"){
      tooltip = self.state.isVisible? <PacchettiTooltip openParentMethod={this.openTooltip} closeParentMethod={this.closeTooltip}/> : <span/>
    }
    else if(this.state.type==="Contatti"){
      tooltip = self.state.isVisible? <ContattiTooltip openParentMethod={this.openTooltip} closeParentMethod={this.closeTooltip}/> :  <span/>
    }
    else if(this.state.type==="Partners"){
      tooltip = self.state.isVisible? <PartnersTooltip openParentMethod={this.openTooltip} closeParentMethod={this.closeTooltip}/> :  <span/> 
    }


    // 
  return (
             <span className="menu-span-element" onMouseOver={self.openTooltip} onMouseLeave={self.closeTooltip} >
                 <button   className={buttonClassName} >{this.state.type}</button>
                    {tooltip}
              </span>
  );
}
}
export default MenuComponent;

