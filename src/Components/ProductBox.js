import '../CSS/ProductBox.css';
import React from 'react';
import ProductDetail from './ProductDetail';


class ProductBox extends React.Component {
  constructor(props) {
    super(props);
    this.mouseSquareOver = this.mouseSquareOver.bind(this);
    this.mouseSquareLeave = this.mouseSquareLeave.bind(this);


    this.state = {
      imgUrl: process.env.PUBLIC_URL + this.props.productProperties.immagine1,
      productInCart: false
    }
  }


  mouseSquareOver() {
    this.props.mouseOverSquareEvent(this.props.productProperties.codice);

  }


  mouseSquareLeave() {
    this.props.mouseLeaveSquareEvent(this.props.productProperties.codice);
  }


  componentDidMount() {
    var self = this;
    //Gestisce posizione tooltip per evitare che non esca orizzontalmente dalla pagina
     var tooltip = document.getElementById("square-div-tooltip" + self.props.productProperties.codice);
     if (!this.isElementInViewport(tooltip)) {
      tooltip.style.left = "-36vw";
    }
  }


  isElementInViewport(el) {
    el.style.display = "block";
    var rect = el.getBoundingClientRect();
    el.style.display = "none";

    return (
      rect.left >= 0 &&
      rect.right <= (window.innerWidth)

    );
  }




  componentWillReceiveProps(newProps) {
    var self = this;
    var newState = Object.assign(self.state);
    newState.imgUrl = process.env.PUBLIC_URL + this.props.productProperties.immagine1;
    newState.productInCart = newProps.productInCart;
    this.setState(newState);
  }




  render() {
    var self = this;
    var tooltipVisibility = this.props.codeProductSelected !== this.props.productProperties.codice ?
      { display: "none" } :
      { display: "block" }

    var titleVisibility = this.props.codeProductSelected !== this.props.productProperties.codice ?
      { display: "block" } :
      { display: "none" }



    var tickVisibility = this.state.productInCart ? { display: "block" } :
      { display: "none" }


    return (
    <div className="global-div">
        <button type="button" className="square-div" id={"square-div" + this.props.productProperties.codice}
          style={{ backgroundImage: "url(" + this.state.imgUrl + ")" }}
          onMouseOver={this.mouseSquareOver}
          onMouseLeave={this.mouseSquareLeave}>
          <div className="tick-class" style={tickVisibility} />
        </button>



        <div className="tooltip-class" onMouseOver={this.mouseSquareOver}
          onMouseLeave={this.mouseSquareLeave} style={tooltipVisibility}
          id={"square-div-tooltip" + self.props.productProperties.codice}>
          <ProductDetail productProperties={self.props.productProperties} addToCart={self.props.addToCart}
            removeToCart={self.props.removeToCart} productInCart={self.state.productInCart} />

        </div>


        <div style={titleVisibility}>
          <div id="codice-prod">{this.props.productProperties.codice}</div>
          <div id="nome-prod">{this.props.productProperties.nome}</div>

          <div id="prezzo-prod">
            {this.props.productProperties.prezzo}
            <div className="euro-symbol">€</div>
          </div>
        </div>

      </div>
    );
  }
}
export default ProductBox;

