import ProductsList from "./ProductsList";
import searchIcon from '../Immagini/search.png';
import '../CSS/Homepage.css';
import TitleAndMenu from './TitleAndMenu';
import FooterLogo from './FooterLogo';
import AnimationAndCategories from './AnimationAndCategories';
import PannelloAdmin from './PannelloAdmin';

import React from 'react';

class Homepage extends React.Component {
  constructor(props){
    super(props);
    this.toggleCategory = this.toggleCategory.bind(this);
    this.changeFilterString = this.changeFilterString.bind(this);
    this.changeFilterCategory = this.changeFilterCategory.bind(this);
    this.togglePromotions = this.togglePromotions.bind(this);
    this.setListaNozzeCode = this.setListaNozzeCode.bind(this);
    this.showAdministratorPanel = this.showAdministratorPanel.bind(this);
    this.closeAdministratorPanel = this.closeAdministratorPanel.bind(this);


    this.state={
                 isLabelShown:true,     
                 filterString:"",
                 filterCategory:"",
                 categorySelectedOver:"",
                 filterProm:false,
                 panelVisible:false,
                 operationPanel:null,
                 listaNozzeCode:""
                } 
  }


  componentDidMount(){
    console.log("Montato!");
    var self=this;
    setInterval(function(){
      if(self.state.isLabelShown)
      self.setState({isLabelShown:false})
      else
      self.setState({isLabelShown:true})
    },500);
  
  }



  changeFilterCategory(e){   
    var self=this;    
    var newState = Object.assign(self.state);

    if(e.target.innerHTML !== self.state.filterCategory){
      newState.filterCategory = e.target.innerHTML;
      if(e.target.innerHTML==="Lista nozze" ){
        var input = prompt(`Digita il codice della lista nozze per accedere ai prodotti di una specifica lista nozze. 
        Lascia vuoto per vedere tutti i prodotti associabili a una lista nozze`);
        self.setListaNozzeCode(input);
      }
    }
    else{
      newState.filterCategory ="";
      newState.listaNozzeCode="";
    }   
    self.setState(newState);  
  }



 toggleCategory(e){
  var self=this;
   var newState = Object.assign(self.state);
   newState.categorySelectedOver = this.state.categorySelectedOver !== e.target.innerHTML ? 
                                    e.target.innerHTML : "";
   self.setState(newState);
 }


  togglePromotions(){
    var self=this;
    var newState = Object.assign(self.state);
    newState.filterProm = !self.state.filterProm;
    self.setState(newState);
    return self.state.filterProm;

  }

  changeFilterString(e){
    var self=this;
    if(e.keyCode===13){
      var newState = Object.assign(self.state);
        newState.filterString = e.target.value;
        self.setState(newState);
    }
  }


  setListaNozzeCode(input){
    var self=this;
    var newState = Object.assign(self.state);
    newState.listaNozzeCode = input;
    self.setState(newState);
  }


  showAdministratorPanel(){
   var passw = prompt("Digita password per accedere al pannello di controllo prodotti");
   if(passw==="MAMMA"){
    var operation = prompt("Digita AGGIUNGI per aggiungere nuovi prodotti; ELIMINA per eliminare nuovi prodotto; AGGIORNA per modificare un prodotto")
    var self=this;
    var newState = Object.assign(self.state);
    newState.panelVisible = true;
    newState.operationPanel = operation;
    self.setState(newState);
   }
   else alert("PASSWORD ERRATA!")
  }

  closeAdministratorPanel(){
    var self=this;
    var newState = Object.assign(self.state);
    newState.panelVisible = false;
    self.setState(newState);

  }

  render(){
    var element=<div></div>;
    var self=this;
    var labelCerca=<div></div>
    var promotionClass = self.state.filterProm? "promotions-selected" : "promotions-unselected"  

    if(this.state.isLabelShown)
      labelCerca =  <div className="label-search">Cerca nel negozio:</div>
    else if(!this.state.isLabelShown)
      labelCerca=<div className="label-search"></div>
    if(this.props.visibleWindow==="Homepage"){

      element=<div>
                  <TitleAndMenu/>
                  <div id="divLogin" onClick={this.showAdministratorPanel}>Login</div>

                  {this.state.panelVisible &&
                      <PannelloAdmin operation={this.state.operationPanel} closePanel={this.closeAdministratorPanel}/>
                  }
                    

                  <div className="central-class">
                      <div className="foto-laterale"></div> 
                      <AnimationAndCategories filterCategory={self.state.filterCategory} categorySelectedOver={self.state.categorySelectedOver} 
                                              toggleCategory={self.toggleCategory} changeFilterCategory={self.changeFilterCategory} 
                                              setListaNozzeCode={self.setListaNozzeCode}/>
                 </div>
                <div className="search-div-class">
                    {labelCerca}
                    <div>
                      <input type="text" className ="search-filter-class" onKeyDown={this.changeFilterString}/>
                      <img src={searchIcon} alt ="Immagine mancante" className="search-icn"/>
                    </div>                      
                </div>                 
                <div>
                    <button  className={promotionClass} onClick={self.togglePromotions}>Promozioni</button>
                </div>
                <div className="products-list-and-footer">
                  <ProductsList filterCategory={self.state.filterCategory} filterString={self.state.filterString} 
                                filterProm = {self.state.filterProm} listaNozzeCode = {self.state.listaNozzeCode}
                                setListaNozzeCode = {self.setListaNozzeCode}/>

                  <FooterLogo/>    
              </div>
            </div>
    }
  return element;
  }
}
export default Homepage;
