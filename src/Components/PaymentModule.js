import '../CSS/PaymentModule.css';
import React from 'react';

class PaymentModule extends React.Component {
  
  constructor(props){
    super(props);  
    this.changePaymentOption=this.changePaymentOption.bind(this);
    this.setInfo=this.setInfo.bind(this);

    this.state={
        nome:"",
        cognome:"",
        email:"",
        cellulare:"",
        quantita:"",
        comune:"",
        provincia:"",
        cap:"",
        indirizzo:"",
        note:"",
        spedizione:"Ritiro in negozio"
    }
  }



  changePaymentOption(e){
      var paymentModeString = e.target.id.split(":")[0];
      this.props.changePaymentSelected(paymentModeString);
      
  }




  setInfo(e){
    var namePropertyToSet =e.target.id==="shipping-1-radio" || e.target.id==="shipping-2-radio" || e.target.id==="shipping-3-radio"?
                            "spedizione" : 
                            e.target.id;
    var self=this;
    var newState = Object.assign(self.state);
    newState[namePropertyToSet] = e.target.value;   
    console.log("STATO INFO: ", self.state);
    self.props.setInfoNotification(self.state);

  }


  render(){
    var simpleCartFields = [
      "Nome","Cognome","Email","Cellulare","Comune","Provincia","Cap","Indirizzo"];
  

  return (
              <form autoComplete="off" className="payment-module">
                    <div className="titolo-payment-form">Modulo di pagamento</div>
                  
                    <div className="total-form">
                    {simpleCartFields.map((field)=>{
                      return (
                        <div className="form-payment">
                          <div className="label-payment">{field}</div>
                          <input type="text" autoComplete="off" id={field.toLowerCase()} className="input-payment" defaultValue={this.state[field.toLowerCase()]} onBlur={this.setInfo}/>
                        </div>
                       )})
                    }

                      <div className="form-payment-shippingMode">
                        <div className="label-shipping-mode">Spedizione</div>
                        <label for="shipping-1-radio" className="label-radio-shipping" >Corriere</label>
                        <input id="shipping-1-radio" value="Corriere"  type="radio" name="shipping-radio" checked={this.state.spedizione === "Corriere"} onChange={this.setInfo}/>
                        <label  for="shipping-2-radio" className="label-radio-shipping">Ritiro in negozio</label>
                        <input   id="shipping-2-radio"  value="Ritiro in negozio" type="radio"  name="shipping-radio" checked={this.state.spedizione === "Ritiro in negozio"} onChange={this.setInfo}/>                
                      </div>
                      <div className="form-payment-paymentMode">
                        <div>Paga con</div>
                        <label for="PayPal:Radio" className="label-radio-payment">Paypal</label>
                        <input defaultChecked id="PayPal:Radio" type="radio" name="pay-with-radio" onChange={this.changePaymentOption}/>
                        <label for="Postepay:Radio" className="label-radio-payment">Postepay</label>
                        <input disabled id="Postepay:Radio" type="radio"  name="pay-with-radio" onChange={this.changePaymentOption}/>
                        <label for="SumUp:Radio" className="label-radio-payment">SumUp</label>
                        <input disabled id="SumUp:Radio" type="radio" name="pay-with-radio" onChange={this.changePaymentOption}/>
                      </div>
                    </div>
              </form>
  );
}
}
export default PaymentModule;

