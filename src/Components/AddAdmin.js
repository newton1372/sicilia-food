import '../CSS/PannelloAdmin.css';
import Properties from '../Properties/NetworkProperties'
import React from 'react';

class AddAdmin extends React.Component {
  constructor(props){
    super(props);
    this.changeStateVariable = this.changeStateVariable.bind(this);
    this.callAddElementService=this.callAddElementService.bind(this);

    this.state={
             nome:null,
             codice:null,
             descrizione:null,
             categorie:null,
             peso:null,
             dimensioni:null,
             prezzo:null,
             brand:null,
             promozione:null,
             quantita:null,
             immagine1:null,
             immagine2:null,
             immagine3:null
      } 
  }


 
   callAddElementService(){
     var self=this;
     const url_add_service="http://"+Properties.public_ip+":"+Properties.port_db_app+"/addNewElement"
     fetch(url_add_service,{
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json'
          // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer',
        body: JSON.stringify(self.state)
      }).then((resp)=>{
        resp.text().then((respText)=>{
          respText === "OK" ? alert("Elemento inserito con successo!") : alert("Operazione fallita!")
        });
    });
   }

   changeStateVariable(e){
    var self=this;
    var newState = Object.assign(self.state);
    e.target.id==="immagine1" || e.target.id==="immagine2" || e.target.id==="immagine3" ?
        newState[e.target.id] = "/Immagini/Homepage/"+e.target.value :  
        newState[e.target.id] = e.target.value;
    self.setState(newState);
   }


  render(){
    var FieldsProducts = [
        "Codice",
        "Nome",
        "Categorie",
        "Descrizione",
        "Brand",
        "Prezzo",
        "Immagine1",
        "Immagine2",
        "Immagine3",
        "Dimensioni",
        "Peso",
        "Promozione",
        "Quantita"
    ]



    return(
      <div className="admin-panel-container">
          <div className="title-panel">Aggiungi nuovo prodotto</div>

          <div className="colonnaLabel">
          {
            FieldsProducts.map((x)=>{
              return <div className="row">
                        <div className="labelField" >{x}:</div>
                      </div>
            }) 
          }           
          </div>
          <div className="colonnaInput">
          {
            FieldsProducts.map((x)=>{
             return <div className="row">
                      <input type="text" className="input-text-admin" id={x.toLowerCase()} onChange={this.changeStateVariable}/>
                    </div>
            }) 
          }           
          </div>
        <div className="footerAdmin">
          <input type="button" value="Aggiungi prodotto" id="submit" onClick={this.callAddElementService}/>
          <input type="button" value="Chiudi" id="close" onClick={this.props.closeAdminPanel}/>
        </div>
    </div>  
    );
  }
}
export default AddAdmin;
