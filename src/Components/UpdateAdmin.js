import '../CSS/PannelloAdmin.css';
import Properties from '../Properties/NetworkProperties'
import React from 'react';

class UpdateAdmin extends React.Component {
  constructor(props){
    super(props);
    this.changeStateVariable = this.changeStateVariable.bind(this);
    this.callUpdateElementService=this.callUpdateElementService.bind(this);
    this.clickAggiorna=this.clickAggiorna.bind(this);

    this.state={
             nome:"",
             codice:"",
             descrizione:"",
             categorie:"",
             peso:"",
             dimensioni: "",
             prezzo: "",
             brand:"",
             promozione:"",
             quantita:"",
             immagine1:"",
             immagine2:"",
             immagine3:""
      } 
  }


  clickAggiorna(e){
    var self=this;
    return new Promise((res,rej)=>{

      self.setState({
        immagine1: "/Immagini/Homepage/"+self.state.immagine1,
        immagine2: "/Immagini/Homepage/"+self.state.immagine2,
        immagine3: "/Immagini/Homepage/"+self.state.immagine3
      })
      res();
    }).then(    
      (prom)=>{
        console.log("CIAO3");

          self.callUpdateElementService();
      }
    );
  }



  callUpdateElementService(){
    var self=this;
    const url_update_service="http://"+Properties.public_ip+":"+Properties.port_db_app+"/updateElement";
    fetch(url_update_service,{
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow', // manual, *follow, error
      referrerPolicy: 'no-referrer',
      body: JSON.stringify(self.state)
    }).then((resp)=> {
      resp.text().then((respText)=>{
      if(respText==="OK") alert("Elemento aggiornato con successo!");
      else alert("Aggiornamento fallito");
    });
   });
  }



   changeStateVariable(e){
    var self=this;
    var newState = Object.assign(self.state);
    newState[e.target.id] = e.target.value;
    self.setState(newState);
   }


   componentDidMount(){
      var codRead = prompt("Digitare il codice del prodotto da modificare");
      var self=this;
      const url_read_by_code_service="http://"+Properties.public_ip+":"+Properties.port_db_app+"/readElementByCode"
      fetch(url_read_by_code_service,{
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json'
          // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer',
        body: JSON.stringify({
          codice:codRead
        })
      }).then((resp)=>{
        resp.json().then((respJSON)=>{
           console.log(respJSON);
           var newState = Object.assign(self.state);
           newState.codice = respJSON.codice;
           newState.nome = respJSON.nome;
           newState.categorie = respJSON.categorie;
           newState.descrizione = respJSON.descrizione;
           newState.quantita = respJSON.quantita;
           newState.brand = respJSON.brand;
           newState.prezzo = respJSON.prezzo;
           newState.immagine1 = respJSON.immagine1? respJSON.immagine1.replace("/Immagini/Homepage/",""):"";
           newState.immagine2 = respJSON.immagine2? respJSON.immagine2.replace("/Immagini/Homepage/",""):"";
           newState.immagine3 = respJSON.immagine3? respJSON.immagine3.replace("/Immagini/Homepage/",""):"";
           newState.categorie = respJSON.categorie;
           newState.peso = respJSON.peso;
           newState.dimensioni = respJSON.dimensioni;
           self.state=newState;
       });
    });
    }
   
   
 
 



  render(){
    var FieldsProducts = [
      "Codice",
      "Nome",
      "Categorie",
      "Descrizione",
      "Brand",
      "Prezzo",
      "Immagine1",
      "Immagine2",
      "Immagine3",
      "Dimensioni",
      "Peso",
      "Promozione",
      "Quantita"
  ]

 

    return(
      <div className="admin-panel-container">
          <div className="title-panel">Aggiorna prodotto</div>

          <div className="colonnaLabel">
          {
            FieldsProducts.map((x)=>{
              return <div className="row">
                        <div className="labelField" >{x}:</div>
                      </div>
            }) 
          }           
          </div>
          <div className="colonnaInput">
          {
            FieldsProducts.map((x)=>{
             return <div className="row">
                      <input type="text" className="input-text-admin" onChange={this.changeStateVariable} id={x.toLowerCase()} value={this.state[x.toLowerCase()]}/>
                    </div>
            }) 
          }           
          </div>
        <div className="footerAdmin">
          <input type="button" value="Aggiorna prodotto" id="submit" onClick={this.clickAggiorna}/>
          <input type="button" value="Chiudi" id="close" onClick={this.props.closeAdminPanel}/>
        </div>
    </div>  
    );
  }

}
export default UpdateAdmin;
