import '../CSS/Pacchetti.css';
import PackagesList from '../Properties/PackagesList';

import React from 'react';

class PacchettiTooltip extends React.Component {
  
  render(){
    
        

  return (
              <div className="pacchetti-tooltip-container" onMouseOver={this.props.openParentMethod} onMouseLeave={this.props.closeParentMethod}>
                <div className="titolo-pacchetti">
                  Pacchetti
                </div>
                <div className="subtitle-pacchetti">
                  Una collezione di offerte tutte per te a un prezzo esclusivo!
                </div>

                <div className="packages-list">
                    {
                      PackagesList.map(function(item){
                        var arrayOfElements = item.elements;
                        console.log(arrayOfElements,"ARRAY");
                        return(
                          <div className="package-container">
                            <div className="description-package">
                              <ul>
                                {
                                  
                                  arrayOfElements.map(function(element){
                                    return <li>{element}</li>
                                  })
                                }
                                <div className="prezzo-package">
                                    {item.price}
                                    <div className="euro-symbol-package">€</div>
                                </div>
                              </ul>
                          
                            </div>
                            <div className="image-package" style={{backgroundImage:"url('.."+item.imageLink+"')"}}/>
                          </div>)
                        })
                    }    
              
                </div>
               </div>                
  );
}
}
export default PacchettiTooltip;

