import React from 'react';
import '../CSS/Homepage.css';
import logoSiciliaFood from '../Immagini/logoSiciliaFood.jpeg';


class FooterLogo extends React.Component {
  constructor(props){
    super(props);    
  } 
  


  
  
  
  render(){
   
  return(
  <div className="footer" >
    <div  className="logo-div" >
      <hr/>
      <img src={logoSiciliaFood}  alt="logo negozio" class="logo-im"/>
      <div className="references">Sicilia Food <br/>Via Solferino <br/>n.156<br/> </div>
    </div>
  </div>
   ); 
  }
}
export default FooterLogo;
