import '../CSS/PannelloAdmin.css';
import AddAdmin from './AddAdmin';
import UpdateAdmin from './UpdateAdmin';
import DeleteAdmin from './DeleteAdmin';
import React from 'react';

class PannelloAdmin extends React.Component {
  constructor(props){
    super(props);
    this.closeAdminPanel=this.closeAdminPanel.bind(this);

    this.state={
          operation:this.props.operation
      } 
  }


  closeAdminPanel(){
      this.props.closePanel();
  }


  render(){
    var isOptionInvalid =   this.state.operation !== "AGGIORNA" &&
                            this.state.operation !== "AGGIUNGI" &&
                            this.state.operation !== "ELIMINA";
    
    if(isOptionInvalid){
      alert("OPZIONE NON VALIDA");
      this.closeAdminPanel();
    }
  
    return(
      <div>
        { this.state.operation === "AGGIUNGI" && 
            <AddAdmin closeAdminPanel={this.closeAdminPanel}/>
        }

        {this.state.operation === "ELIMINA" && 
            <DeleteAdmin closeAdminPanel={this.closeAdminPanel}/>
         }

        {this.state.operation === "AGGIORNA" &&
            <UpdateAdmin closeAdminPanel={this.closeAdminPanel}/>
        }
        

      </div>
    );
        
    }
  
}
export default PannelloAdmin;
