import '../CSS/Homepage.css';
import React from 'react';
import CategoryList from '../Properties/CategoryList';
import '../CSS/HomepageAnimation.css';



class AnimationAndCategories extends React.Component {
  constructor(props){
    super(props);
    this.manageCategoryClick = this.manageCategoryClick.bind(this);

    this.state={isLabelShown:true,     
      filterCategory:"",
      categorySelectedOver:""
    } 

  }





  componentWillReceiveProps(newProps){
    var self=this;
    var newState = Object.assign(self.state);
    
    if(self.state.filterCategory!==newProps.filterCategory){
            newState.filterCategory = newProps.filterCategory;
    }
    newState.categorySelectedOver = newProps.categorySelectedOver;  
    self.setState(newState);

  }



  manageCategoryClick(e){
    this.props.changeFilterCategory(e);
  }



  render(){
    var self=this;  
    return (      
                    <div>
                          <div className="category-list-class">
                                {    
                                  
                                  CategoryList.listOfLeftCategories.map(function(item){
                                    var styleCategory = (item === self.state.categorySelectedOver || item===self.state.filterCategory) ?
                                                "category-button-selected" : "category-button-unselected";
                                  return (                             
                                      <button  className={styleCategory} onMouseOver={self.props.toggleCategory} onMouseLeave={self.props.toggleCategory} 
                                               onClick={self.props.changeFilterCategory}>{item}</button>
                                  )
                                })
                                }
                          </div>
                        {  /*CERCA NEL CSS HOMEPAGEANIMATION.CSS */  }                       
                          <div className="animation-class">
                              <div  className="animation-div"/>
                          </div>    
                          <div className="category-list-class-right">
                                {    
                                  CategoryList.listOfRightCategories.map(function(item){
                                    var styleCategory = (item === self.state.categorySelectedOver || item===self.state.filterCategory) ?
                                    "category-button-selected" : "category-button-unselected";
                                  return (                             
                                      <button  className={styleCategory} onMouseOver={self.props.toggleCategory} onMouseLeave={self.props.toggleCategory} onClick={self.manageCategoryClick}>{item}</button>
                                  )
                                })
                                }
                          </div>         
                  </div>);
  }

}
export default AnimationAndCategories;
