const CategoryList = {
  listOfLeftCategories: [
    "Biscotti",
    "Cannoli siciliani",
    "Granite",
    "Pasticceria",
    "Panini",
    "Formaggi tipici",
    "A' sasizza"
  ],
  listOfRightCategories: [
    "Snack e frutta secca",
    "Vini locali",
    "Souvenir",
    "Tavola calda",
    "Quadri",
    "Frutta e verdura",
    "Franchising"
  ]
}

export default CategoryList